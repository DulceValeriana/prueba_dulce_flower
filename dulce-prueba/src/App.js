import React, { Component, addReducer } from "reactn";
import {  Router, Route, Switch } from "react-router-dom";
import Login from "./container/Login";
import Dashboard from "./container/Dashboard";
import CrearUsuario from "./container/CrearUsuario";
import history from './browserHistory';
import './App.css';
import Aside from './component/Aside';
import Lateral from './component/Lateral';

import 'bootstrap/dist/css/bootstrap.min.css';
// import 'bootstrap/dist/js/bootstrap.min.js';

function App() {
  return (
  
    <div>
      <Router history={history}>
        <Aside/>
        <Lateral/>
        <Switch>
          <Route exact path="/" component={Login} />
          <Route exact path="/dashboard" component={Dashboard} />
          <Route exact path="/CrearUsuario" component={CrearUsuario} />
        </Switch>
      </Router>
    </div>

  );
}

export default App;
