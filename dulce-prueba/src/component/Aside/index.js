// eslint-disable-next-line
import React,{useState, useGlobal}  from "reactn";
import avatar from "../../assets/img/user-female.svg";
import history from "../../browserHistory";
import '../../assets/css/aside.css';


function Aside(props) {

    const handleSubmit = ()=> {
        history.push("/");
    }
    
  return (window.location.pathname == "/" || window.location.pathname == "/#" ? 
    <div>

    </div> 
    :
    <div className="bg-aside">
        <div className="col-md-4">
            <div className="row">
                <div className="col-sm-2 class-user-dashboard">
                    <img src={avatar} alt="Avatar" class="avatar"/>
                </div>
                <div className="col-sm-10">
                    <div class="dropdown">
                        <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Tu nombre usuario
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#" onMouseDown={handleSubmit}>Cerrar sesion</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  );
}

export default Aside;

