// eslint-disable-next-line
import React  from "reactn";
import avatar from "../../assets/img/user-female.svg";
import history from "../../browserHistory";
import '../../assets/css/lateral.css';


function Lateral(props) {

    const handleUser = ()=> {
        history.push("/dashboard");
    }

    const handleCreateUser = ()=> {
        history.push("/CrearUsuario");
    }

  return (window.location.pathname == "/" || window.location.pathname == "/#" ? <div>
      
  </div> :
    <div className="bg-lateral col-md-2">
        <div className="row">
            <ul>
                <li onMouseDown={handleUser}><i class="fa fa-user" aria-hidden="true"></i> <h5>Usuarios</h5></li>
                <li onMouseDown={handleCreateUser}><i class="fa fa-user-plus" aria-hidden="true"></i> <h5>Crear Usuarios</h5></li>
            </ul>
        </div>
    </div>
  );
}

export default Lateral;

