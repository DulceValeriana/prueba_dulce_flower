// eslint-disable-next-line
import React, {useGlobal} from "reactn";
import '../../assets/css/dashboard.css';
// Components

//Others
import history from "../../browserHistory";
// import validateFields from "../../helpers/informed/validators";

function SurveyDashboard(props) {
  const [users, setUsers] = useGlobal("user");
  
  return (
    <div className="col-sm-10 offset-sm-2 class-dashboard">
      <div className="col-sm-12">
        <h1>Lista de usuarios</h1>
      </div>
      <div className="col-sm-12">
        <table className="table">
          <thead>
            <tr>
              <th>ID</th>
              <th>Nombre completo</th>
              <th>Correo</th>
              <th>Correo electrónico</th>
              <th>Género</th>
            </tr>
          </thead>
          <tbody>
            <SurveyRow name ="carlos" username="trololo" email="thisemail@is.basic" type="subscriber" active={true} />
            <SurveyRow name ="carlos" username="trololo" email="thisemail@is.basic" type="subscriber" active={true} />
            <SurveyRow name ="carlos" username="trololo" email="thisemail@is.basic" type="subscriber" active={true} />
            <SurveyRow name ="carlos" username="trololo" email="thisemail@is.basic" type="subscriber" active={true} />
            <SurveyRow name ="carlos" username="trololo" email="thisemail@is.basic" type="subscriber" active={true} />
          </tbody>
        </table>
      </div>
    </div>
  );
}

export default SurveyDashboard;


function SurveyRow(props){


  return(
    <tr>
      <td>{props.name}</td>
      <td>{props.username}</td>
      <td>{props.email}</td>
      <td>{props.type}</td>
      <td>{props.active ? "activo" : "inactivo"}</td>
    </tr>


  )

}