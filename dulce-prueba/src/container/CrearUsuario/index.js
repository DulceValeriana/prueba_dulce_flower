import React from "reactn";
import history from "../../browserHistory";
import '../../assets/css/style.css';

export default function CrearUsuario(props) {
    const handleCreateUser = ()=> {
        history.push("/dashboard");
    }
    return (
    <div className="col-sm-10 offset-sm-2">
        <form className="row">
            <div className="col-sm-12">
                <h1>Crear usuarios</h1>
            </div>
            <div className="form-group col-sm-4">
                <input type="text" className="form-control" id="formGroupExampleInput" placeholder="Nombre"/>
            </div>
            <div className="form-group col-sm-4">
                <input type="text" className="form-control" id="formGroupExampleInput2" placeholder="Primer apellido"/>
            </div>
            <div className="form-group col-sm-4">
                <input type="text" className="form-control" id="formGroupExampleInput" placeholder="Segundo apellido"/>
            </div>
            <div className="form-group col-sm-4">
                <input type="text" className="form-control" id="formGroupExampleInput2" placeholder="Cédula"/>
            </div>
            <div className="form-group col-sm-4">
                <input type="text" className="form-control" id="formGroupExampleInput" placeholder="Edad"/>
            </div>
            <div className="form-group col-sm-4 white-text">
                <div className="row">
                    <div className="col-sm-12">
                        <label for="staticEmail" class="p-0 col-form-label">Genero</label>
                    </div>
                    <div className="form-check col-sm-6">
                        <input className="form-check-input" type="radio" name="exampleRadios" id="exampleRadios1" value="option1" checked/>
                        <label className="form-check-label" for="exampleRadios1">
                            Hombre
                        </label>
                    </div>
                    <div className="form-check col-sm-6">
                        <input className="form-check-input" type="radio" name="exampleRadios" id="exampleRadios2" value="option2"/>
                        <label className="form-check-label" for="exampleRadios2">
                            Mujer
                        </label>
                    </div>
                </div>
            </div>
            <div className="form-group col-sm-4">
                <input type="text" className="form-control" id="formGroupExampleInput2" placeholder="Teléfono"/>
            </div>
            <div className="form-group col-sm-4">
                <input type="text" className="form-control" id="formGroupExampleInput" placeholder="Correo electrónico"/>
            </div>
            <div className="form-group col-sm-4">
                <div class="form-group">
                    <select class="form-control" id="exampleFormControlSelect1">
                        <option>Estado Civil</option>
                        <option>Soltero/a</option>
                        <option>Casado/a</option>
                        <option>Viudo/a</option>
                    </select>
                </div>
            </div>
            <div className="form-group col-sm-4 white-text">
                <div className="row">
                    <div className="col-sm-12">
                        <label for="staticEmail" class="p-0 col-form-label">Tienes hijos?</label>
                    </div>
                    <div className="form-check col-sm-6">
                        <input className="form-check-input" type="radio" name="exampleRadios" id="exampleRadios1" value="option1" checked/>
                        <label className="form-check-label" for="exampleRadios1">
                            Si
                        </label>
                    </div>
                    <div className="form-check col-sm-6">
                        <input className="form-check-input" type="radio" name="exampleRadios" id="exampleRadios2" value="option2"/>
                        <label className="form-check-label" for="exampleRadios2">
                            No
                        </label>
                    </div>
                </div>
            </div>
            <div className="form-group col-sm-4">
                <input type="date" className="form-control" id="formGroupExampleInput2" placeholder="Fecha de nacimiento"/>
            </div>
            <div className="form-group col-sm-4">
                <input type="text" className="form-control" id="formGroupExampleInput" placeholder="Dirección 1"/>
            </div>
            <div className="form-group col-sm-4">
                <input type="text" className="form-control" id="formGroupExampleInput" placeholder="Dirección 2"/>
            </div>
            <div className="form-group col-sm-4">
                <input type="text" className="form-control" id="formGroupExampleInput" placeholder="Dirección 3"/>
            </div>
            <div className="form-group col-sm-4">
                <input type="text" className="form-control" id="formGroupExampleInput" placeholder="Dirección 4"/>
            </div>
            <div className="col-sm-2">
                <button type="button" class="btn btn-light" onMouseDown={handleCreateUser}>Cancelar</button>
            </div>
            <div className="col-sm-2 offset-sm-8">
                <button type="button" class="btn btn-success" onMouseDown={handleCreateUser}>Guardar</button>
            </div>
        </form>
    </div>
    );
  }
  