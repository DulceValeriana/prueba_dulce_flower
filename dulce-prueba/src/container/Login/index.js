import React,{ useGlobal} from "reactn";
import history from "../../browserHistory";
import avatar from "./user-female.svg";
import '../../assets/css/style.css';

export default function Login(props) {
  const [test, setTest] = useGlobal("test");
    const handleSubmit = ()=> {
       history.push("/dashboard");
       let ref = [...test];
        ref.push(Math.random());
        setTest(ref);
        console.warn(test);
    }

    return (
      
    <div className="body">
      
      <form class="modal-content-2 animate" action="/action_page.php" method="post">
        <div class="imgcontainer">
          <img src={avatar} alt="Avatar" class="avatar"/>
        </div>
    
        <div class="container">
          <label for="uname"><b>Username</b></label>
          <input type="text" placeholder="Enter Username" name="uname" required/>
    
          <label for="psw"><b>Password</b></label>
          <input type="password" placeholder="Enter Password" name="psw" required/>
            
          <button onMouseDown={handleSubmit} type="submit">Login</button>
    
        </div>
      </form>
    </div>
    );
  }
  